extends Node3D

const SPEED := 30.0

var dir: Vector3
var activated := false

func activate(dir: Vector3):
	self.dir = dir
	activated = true

func _process(delta):
	if !activated:
		return
	
	position += dir * SPEED * delta
