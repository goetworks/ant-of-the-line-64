extends CharacterBody3D

@export var jump_height := 5
@export var jump_horizontal := .5
@export var react_distance := 5
@export var gravity_multiplier := 1.0

var time_since_last_whine := 0.0
var max_whine_pause = 300.0
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var inverter: Vector2

func _process(delta):
	if !Globals.player:
		return
	
	if !is_on_floor():
		velocity.y -= gravity * gravity_multiplier * delta
	
	var dist = global_position.distance_to(Globals.player.global_position)
	if dist < react_distance:
		yipee()
	else:
		velocity.x = 0
		velocity.z = 0
		whine(delta)

	move_and_slide()
	

func yipee():
	if is_on_floor():
		jump()
		$JumpParticles.restart()
	look_at(Globals.player.global_position)

func whine(delta: float):
	if $Whine.playing:
		return
	
	time_since_last_whine += delta
	if randf_range(0.0, max_whine_pause) <= time_since_last_whine:
		$Whine.pitch_scale = randf_range(0.9, 1.1)
		$Whine.play()
		time_since_last_whine = 0.0

func jump():
	$JumpSound.play()
	velocity.y = jump_height

	var horizontal = _get_horizontal()
	velocity.x = horizontal.x
	velocity.z = horizontal.y
	
func _get_horizontal() -> Vector2:
	if !inverter.is_zero_approx():
		var opposite = -1 * inverter
		inverter = Vector2.ZERO
		return opposite
	
	inverter = Vector2(randf_range(-1, 1), randf_range(-1, 1)).normalized() * jump_horizontal
	return inverter


func _on_area_3d_body_entered(body):
	if body is Ant:
		body.add_nymph()
		queue_free()
