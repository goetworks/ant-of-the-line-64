extends ColorRect

@export var transition_time := 3
@onready var label_2 = $"../Label2"

var done := true

func _ready():
	fade_in()

func fade_in():
	done = false
	var tween = get_tree().create_tween()
	tween.tween_property(self, "color", Color(0, 0, 0, 0), transition_time)
	tween.tween_callback(fade_in_cleanup)

func fade_in_cleanup():
	done = true
	label_2.playing = true

func fade_out():
	done = false
	var tween = get_tree().create_tween()
	tween.tween_property(self, "color", Color(0, 0, 0, 1.0), transition_time).set_trans(Tween.TRANS_CUBIC)
	tween.tween_callback(Globals.begin)
