extends Node3D

@export var rotation_speed := 10.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_y(rotation_speed * delta)
