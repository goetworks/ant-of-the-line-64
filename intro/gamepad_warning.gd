extends CanvasLayer

@export var time_visible := 5.0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_visible -= delta
	
	if time_visible <= 0.0:
		Globals.load_intro()
