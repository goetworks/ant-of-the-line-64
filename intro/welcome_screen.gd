extends Node3D

@onready var black_screen = $CanvasLayer/BlackScreen
@onready var label_2 = $CanvasLayer/Label2
@onready var hit = $Hit


func _input(event):
	if event is InputEventKey || event is InputEventJoypadButton:
		if black_screen.done:
			black_screen.fade_out()
			label_2.playing = false
			label_2.visible = false
			hit.play()
