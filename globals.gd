extends Node

const NYMPH_COUNT := 9

var player: Ant
var gui: Node
var score: int

var dead := false

var main_scene = preload("res://test_level.tscn")
var intro_scene = preload("res://welcome_screen.tscn")
var ending_scene = preload("res://ending/endings.tscn")

func _process(delta):
	if Input.is_action_just_pressed("Exit"):
		get_tree().quit()

func load_intro():
	get_tree().change_scene_to_packed(intro_scene)

func begin():
	get_tree().change_scene_to_packed(main_scene)

func frame_freeze(scale, duration):
	Engine.time_scale = scale
	await get_tree().create_timer(duration * scale).timeout
	Engine.time_scale = 1.0

func game_over():
	dead = true
	gui.show_game_over()

func restart():
	get_tree().reload_current_scene()
	dead = false

func roll_ending():
	gui.begin_ending_transition()

func load_ending():
	get_tree().change_scene_to_packed(ending_scene)
