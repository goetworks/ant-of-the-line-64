extends Label

@export var blink_interval := 1.0

var decay := 0.0
var playing = false

func _process(delta):
	if !playing:
		return
	
	decay -= delta
	
	if decay <= 0.0:
		visible = !visible
		decay = blink_interval
