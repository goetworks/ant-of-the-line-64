extends Node3D

@export var nymph_y_offset := -0.5
@export var dead_nymph_scene: PackedScene

@onready var crude_animator = $"../CrudeAnimator"

func _process(delta):
	position.y = nymph_y_offset if crude_animator.flipper && crude_animator.running else 0.0

func get_nymph_count() -> int:
	return get_children().filter(func(x): return x.visible).size()

func add_nymph():
	for child in get_children():
		if !child.visible:
			child.visible = true
			return

func disperse():
	var root = get_tree().get_root()
	for child in get_children():
		if child.visible:
			var scene = dead_nymph_scene.instantiate()
			root.add_child(scene)
			scene.global_position = global_position
			scene.global_rotation = Vector3(randf() * 180, randf() * 180, randf() * 180)
			var dir = Vector3(randf_range(-1, 1), 0.5, randf_range(-1, 1)).normalized()
			scene.activate(dir)
