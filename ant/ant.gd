class_name Ant
extends CharacterBody3D
signal nymph_got(count)

@export var SPEED = 8.0
@export var JUMP_VELOCITY = 4.5
@export var jump_point_velocity = 4.5
@export var grav_multiplier = 1.0
@export var weight = 1.0

@export var dead_scene: PackedScene

@onready var crude_animator = $CrudeAnimator
@onready var nymphs_holder = $NymphsHolder


var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var in_air_last_frame = false
var step_sound_should_play = false
var in_jump_point := false

func _ready():
	Globals.player = self

func _physics_process(delta):
	crude_animator.running = false
	if not is_on_floor():
		velocity.y -= gravity * delta * grav_multiplier

	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
	
	if in_jump_point:
		velocity.y += jump_point_velocity * delta

	var input_dir = Input.get_vector("west", "east", "north", "south").normalized()
	var direction = Vector3(input_dir.x, 0, input_dir.y)
	if direction:
		look_at(global_position + direction)
	
	if is_on_floor():
		_ground_move(direction, delta)
		if in_air_last_frame:
			$LandParticles.restart()
			$LandStream.play()
	else:
		_apply_air_resistance(direction, delta)
		$DriftParticles.emitting = false
	
	in_air_last_frame = !is_on_floor()
	if move_and_slide():
		var collision = get_last_slide_collision()
		var collider = collision.get_collider()
		if collider is DeathZone:
			die(collision.get_normal())
		elif collider is FloatingPlatform:
			collider.push_down(weight)

func _ground_move(direction: Vector3, dt: float):
	var drifting = direction.angle_to(velocity) > 1.0
	$DriftParticles.emitting = drifting
	if drifting:
		play_drift()
	if direction:
		play_step()
		crude_animator.running = true
		velocity.x += direction.x * SPEED * dt * 7.0
		velocity.z += direction.z * SPEED * dt * 7.0
		var y_cache = velocity.y
		velocity = velocity.limit_length(SPEED)
		velocity.y = y_cache
	else:
		velocity.x = lerp(velocity.x, direction.x * SPEED, dt * 7.0)
		velocity.z = lerp(velocity.z, direction.z * SPEED, dt * 7.0)

func _apply_air_resistance(direction: Vector3, dt: float):
	velocity.x = lerp(velocity.x, direction.x * SPEED, dt * 2.0)
	velocity.z = lerp(velocity.z, direction.z * SPEED, dt * 2.0)

func add_nymph():
	nymphs_holder.add_nymph()
	$NymphGotSource.play()
	emit_signal("nymph_got", nymphs_holder.get_nymph_count())
	Globals.score = nymphs_holder.get_nymph_count()

func die(dir: Vector3):
	Globals.frame_freeze(0.05, 0.5)
	Globals.game_over()
	nymphs_holder.disperse()
	var scene = dead_scene.instantiate()
	get_tree().current_scene.add_child(scene)
	scene.global_position = global_position
	scene.global_rotation = global_rotation
	scene.animate(dir, nymphs_holder.get_nymph_count())
	queue_free()

func play_step():
	if !$StepStream.playing:
		$StepStream.pitch_scale = randf_range(0.9, 1.1)
		$StepStream.play()

func play_drift():
	if !$DriftStream.playing:
		$DriftStream.pitch_scale = randf_range(0.9, 1.1)
		$DriftStream.play()

func _on_area_3d_body_entered(body):
	if body != self:
		return
	in_jump_point = true


func _on_area_3d_body_exited(body):
	if body != self:
		return
	in_jump_point = false
