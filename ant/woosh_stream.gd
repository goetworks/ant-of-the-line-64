extends AudioStreamPlayer

@export var min_pitch := 0.2
@export var max_pitch := 0.8
@export var top_speed := 30.0

@onready var ant = $".."

func _process(delta):
	var t = ant.velocity.length() / top_speed
	var w = ease(t, -2)
	pitch_scale = lerp(min_pitch, max_pitch, w)
	volume_db = lerp(-32, -5, w)
