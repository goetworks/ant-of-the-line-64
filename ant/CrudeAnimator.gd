extends Node3D

@export var s_between_steps := 0.5
@export var default: Node3D
@export var step1: Node3D
@export var step2: Node3D

var running := false
var flipper := false
var timer := 0.0

func _ready():
	_set_default()

func _process(delta):
	if !running:
		_set_default()
		return
	
	if timer == 0.0:
		_flip_steps()
	
	timer += delta
	
	if timer >= s_between_steps:
		timer = 0.0

func _set_default():
	default.visible = true
	step1.visible = false
	step2.visible = false

func _flip_steps():
	default.visible = false
	flipper = !flipper
	step1.visible = flipper
	step2.visible = !flipper
