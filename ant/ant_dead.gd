extends RigidBody3D

@export var death_jump_strength := 6

var landed := false

func animate(dir: Vector3, had_nymphs: bool):
	apply_impulse(dir * death_jump_strength)
	$DeathSound.play()
	await get_tree().create_timer(0.1).timeout
	if had_nymphs:
		$NymphDeath.play()


func _on_body_entered(body):
	$JumpParticles.restart()
