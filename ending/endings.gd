extends CanvasLayer

@export var fade_in_time := 3.0

@onready var best = $Best
@onready var normal = $Normal
@onready var worst = $Worst


func _ready():
	$WhiteCover.visible = true
	var fade_in = get_tree().create_tween()
	fade_in.tween_property($WhiteCover, "color", Color(1.0, 1.0, 1.0, 0.0), fade_in_time)
	fade_in.tween_callback(show_ending)

func show_ending():
	var ending = $Worst
	if Globals.score == 9:
		ending = $Best
	elif Globals.score >= 1:
		ending = $Normal
	
	ending.visible = true
	await get_tree().create_timer(2.0).timeout
	$Hit.play()
	ending.show_text()
