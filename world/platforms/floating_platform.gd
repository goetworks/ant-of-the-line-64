class_name FloatingPlatform
extends RigidBody3D

@export var correction_strength := 1.0

var original_pos: Vector3


func _ready():
	original_pos = global_position

func _physics_process(delta):
	linear_velocity.x = 0
	linear_velocity.z = 0
	if position.y < original_pos.y:
		linear_velocity.y += correction_strength * delta
	else:
		linear_velocity.y = 0


func push_down(weight: float):
	apply_central_force(Vector3.DOWN * weight)
