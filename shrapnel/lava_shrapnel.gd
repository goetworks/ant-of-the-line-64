extends CharacterBody3D

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var explosion = preload("res://shrapnel/shrapnel_explosion.tscn")


func _physics_process(delta):
	if not is_on_floor():
		velocity.y -= gravity * delta

	if move_and_slide():
		var collision = get_last_slide_collision()
		spawn_explosion(collision)
		queue_free()

func spawn_explosion(collision: KinematicCollision3D):
	var node = explosion.instantiate()
	node.global_position = collision.get_position()
	get_tree().current_scene.add_child(node)
