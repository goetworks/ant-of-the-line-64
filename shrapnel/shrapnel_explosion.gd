extends Node3D

@export var hurt_time := 1.0

var timer := 0.0
var hot := true

func _ready():
	$GPUParticles3D.restart()

func _process(delta):
	if timer >= 10.0:
		queue_free()
	
	timer += delta
	hot = timer <= hurt_time

func _on_area_3d_body_entered(body):
	if !hot || !body is Ant:
		return
	
	body.die(Vector3.UP)
