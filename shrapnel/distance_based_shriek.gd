extends AudioStreamPlayer

@onready var lava_shrapnel = $".."

const max_distance := 200

func _process(delta):
	if Globals.player == null:
		return
	
	var distance = Globals.player.global_position.distance_to(lava_shrapnel.global_position)
	var t = (max_distance - distance) / max_distance
	var w = ease(t, -2.0)
	pitch_scale = lerp(2.0, 0.1, w)
	volume_db = lerp(-16, -2, w)
