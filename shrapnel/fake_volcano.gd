extends Node3D

@export var start_grace := 10.0
@export var min_time_between := 3.0
@export var max_time_between := 30.0
@export var spawn_height := 200

var shrapnel_packed = preload("res://shrapnel/lava_shrapnel.tscn")
var time_between := 0.0


func _process(delta):
	if !Globals.player:
		return
	
	time_between += delta
	if start_grace > 0 && time_between < start_grace:
		return
	
	start_grace = 0
	if time_between <= min_time_between:
		return
	
	var roll = randf_range(0.0, max_time_between)
	if roll <= time_between:
		fire()
		time_between = 0.0

func fire():
	$AudioStreamPlayer.pitch_scale = randf_range(0.9, 1.1)
	$AudioStreamPlayer.play()
	var spawn_pos = Globals.player.global_position + Vector3(0, spawn_height, 0)
	await get_tree().create_timer(1.0).timeout
	var node = shrapnel_packed.instantiate()
	node.global_position = spawn_pos
	get_tree().current_scene.add_child(node)
