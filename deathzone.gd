class_name DeathZone
extends StaticBody3D

func _process(delta):
	if Globals.dead:
		for child in get_children():
			child.disabled = true
