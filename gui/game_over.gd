extends ColorRect

@export var fade_in_time: float

var ready_to_restart := false

func _ready():
	color.a = 0
	$Labels.visible = false

func _input(event):
	if !ready_to_restart:
		return
	
	if event is InputEventKey || event is InputEventJoypadButton:
		Globals.restart()

func fade_in():
	var tween = get_tree().create_tween()
	tween.tween_property(self, "color", Color.BLACK, fade_in_time)
	tween.tween_callback(tween_end)
	$Music.play()

func tween_end():
	$Labels.visible = true
	ready_to_restart = true
