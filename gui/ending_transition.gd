extends ColorRect

@export var fade_in_time := 3.0

func _ready():
	color.a = 0

func fade_in():
	var tween = get_tree().create_tween()
	tween.tween_property(self, "color", Color.WHITE, fade_in_time).set_ease(Tween.EASE_IN_OUT)
	tween.tween_callback(Globals.load_ending)
