extends HBoxContainer

func _ready():
	set_counter(0)

func set_counter(count: int):
	$Label.text = '%s/%s' % [count, Globals.NYMPH_COUNT]
