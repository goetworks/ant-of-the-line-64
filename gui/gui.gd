extends CanvasLayer

@onready var nymph_counter = $NymphCounter
@onready var game_over = $GameOver
@onready var ending_transition = $EndingTransition


func _ready():
	Globals.gui = self

func show_game_over():
	game_over.fade_in()

func begin_ending_transition():
	ending_transition.fade_in()

func _on_ant_nymph_got(count):
	nymph_counter.set_counter(count)
